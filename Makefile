### * Setup

TOP_DIR=$(shell git rev-parse --show-toplevel)

### ** Colors

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** site

## site : build the website locally
.PHONY: site
site:
	@printf "\n"
	@printf "$(GREEN)*** Building the website ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'rmarkdown::render_site()'

### ** clean

## clean : delete automatically generated files
.PHONY: clean
clean:
	@printf "\n"
	@printf "$(GREEN)*** Deleting automatically generated files ***$(NC)\n"
	@printf "\n"
	@Rscript -e 'rmarkdown::clean_site()'
